package com.javarush.test.level24.lesson02.home01;

/**
 * Created by Анатолий on 18.08.2016.
 */
public class SelfInterfaceMarkerImpl implements SelfInterfaceMarker
{
    public SelfInterfaceMarkerImpl(){}
    public void printSmthng(String text){
        System.out.println(text);
    }
    public int increment(int i){
        return ++i;
    }
}
