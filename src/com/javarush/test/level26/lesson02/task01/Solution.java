package com.javarush.test.level26.lesson02.task01;

import java.util.*;

/* Почитать в инете про медиану выборки
Реализовать логику метода sort, который должен сортировать данные в массиве по удаленности от его медианы
Вернуть отсортированный массив от минимального расстояния до максимального
Если удаленность одинаковая у нескольких чисел, то выводить их в порядке возрастания
*/
public class Solution {
    public static Integer[] sort(Integer[] array) {
        List<Integer> list = Arrays.asList(array);
        Collections.sort(list);
        final int median;
        if (list.size()%2 != 0)
        {
             median = list.get(list.size() / 2);
        }
        else {median = (list.get(list.size() / 2) + list.get((list.size() / 2) - 1))/2;}
        Comparator<Integer> compareByMedian = new Comparator<Integer>() {
            public int compare(Integer o1, Integer o2) {
                if (Math.abs(median-o1)>Math.abs(median-o2)){
                    return (o1-median);
                }
                else if (Math.abs(median-o1)==Math.abs(median-o2)){
                    return o1 -o2;
                }
                else
                return (o2-median);
            }
        };
        Collections.sort(list,compareByMedian);
        array = (Integer[])list.toArray();
        //implement logic here
        return array;
    }

    public static void main(String[] args)
    {
        Integer[] integers = new Integer[6];
        integers[0] = 1;
        integers[1] = 21;
        integers[2] = 7;
        integers[3] = 6;
        integers[4] = 5;
        integers[5] = 2;
//        integers[0] = 3;
//        integers[1] = 5;
//        integers[2] = 5;
//        integers[3] = 7;
//        integers[4] = 11;
        Solution.sort(integers);
        for (int i = 0; i < integers.length; i++)
        {
            System.out.println(integers[i]);
        }
    }
}
