package com.javarush.test.level22.lesson09.task01;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/* Обращенные слова
В методе main с консоли считать имя файла, который содержит слова, разделенные пробелами.
Найти в тексте все пары слов, которые являются обращением друг друга. Добавить их в result.
Порядок слов first/second не влияет на тестирование.
Использовать StringBuilder.
Пример содержимого файла
рот тор торт о
о тот тот тот
Вывод:
рот тор
о о
тот тот
*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
        String content = fileReader.readLine();
        StringBuilder sb = new StringBuilder();

        while (content != null)
        {
            sb.append(content);
            sb.append(" ");
            content = fileReader.readLine();
        }
        fileReader.close();
        reader.close();
        content = sb.toString();
        ArrayList<String> list = new ArrayList<>();
        String[] array = content.split(" ");
        list.addAll(Arrays.asList(array));
        for (int i = 0; i < list.size(); i++)
        {
            StringBuilder temp = new StringBuilder(list.get(i));
            String first = temp.reverse().toString();
            for (int j = i+1; j < list.size(); j++)
            {
                if ((list.get(j).equals(first)) && !list.get(j).equals(" ")){

                        Pair pair = new Pair();
                        pair.first = list.get(i);
                        pair.second = list.get(j);
                        result.add(pair);
                    list.set(i," ");
                    list.set(j," ");
                        j = list.size()-1;
                }
                temp.reverse();
            }
        }

        for (int i = 0; i < result.size(); i++)
        {
            System.out.println(result.get(i).toString());
        }
    }

    public static class Pair {
        String first;
        String second;

        @Override
        public String toString() {
            return  first == null && second == null ? "" :
                    first == null && second != null ? second :
                    second == null && first != null ? first :
                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}
