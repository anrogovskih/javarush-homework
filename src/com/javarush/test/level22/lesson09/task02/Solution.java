package com.javarush.test.level22.lesson09.task02;


import java.util.LinkedHashMap;
import java.util.Map;

/* Формируем Where
Сформируйте часть запроса WHERE используя StringBuilder.
Если значение null, то параметр не должен попадать в запрос.
Пример:
{"name", "Ivanov", "country", "Ukraine", "city", "Kiev", "age", null}
Результат:
"name = 'Ivanov' and country = 'Ukraine' and city = 'Kiev'"
*/
public class Solution {

    public static StringBuilder getCondition(Map<String, String> params) {
        StringBuilder sb = new StringBuilder();
        if ((params != null) && !(params.size() <1))
        {
            for (Map.Entry<String, String> pair : params.entrySet())
            {
                if (pair.getKey() != null && pair.getValue() != null)
                {
                    sb.append(pair.getKey() + " = '" + pair.getValue() + "'");
                    sb.append(" and ");
                }
            }
            if (sb.length()>0)
            {
                sb.replace(sb.lastIndexOf(" and "), sb.length(), "");
            }
        }
        return sb;
    }
    public static void main (String args[]){
        Map<String,String> params = new LinkedHashMap<>();
        params.put("name", "Ivanov");
        params.put("country", "Ukraine");
        params.put("height", "185");
        Map<String,String> params1 = new LinkedHashMap<>();
        params1.put(null, null);
        params1.put("country", "Ukraine");
        params1.put(null, null);
        Map<String,String> params2 = new LinkedHashMap<>();
        params2.put("country", "Ukraine");
        params2.put(null, null);
        params2.put("vvv", "TTT");
        params2.put(null, null);
        params2.put("height", "185");
        Map<String,String> params3 = new LinkedHashMap<>();
        params3.put(null, null);
        params3.put(null, null);
        Map<String,String> params4 = new LinkedHashMap<>();
        System.out.println(getCondition(params).toString());
        System.out.println(getCondition(params1).toString());
        System.out.println(getCondition(params2).toString());
        System.out.println(getCondition(params3).toString());
        System.out.println(getCondition(params4).toString());
    }
}
