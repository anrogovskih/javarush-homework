package com.javarush.test.level22.lesson13.task03;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* Проверка номера телефона
Метод checkTelNumber должен проверять, является ли аргумент telNumber валидным номером телефона.
Критерии валидности:
1) если номер начинается с '+', то он содержит 12 цифр
2) если номер начинается с цифры или открывающей скобки, то он содержит 10 цифр
3) может содержать 0-2 знаков '-', которые не могут идти подряд
4) может содержать 1 пару скобок '(' и ')'  , причем если она есть, то она расположена левее знаков '-'
5) скобки внутри содержат четко 3 цифры
6) номер не содержит букв
7) номер заканчивается на цифру

Примеры:
+380501234567 - true
+38(050)1234567 - true
+38050123-45-67 - true
050123-4567 - true

+38)050(1234567 - false
+38(050)1-23-45-6-7 - false
050ххх4567 - false
050123456 - false
(0)501234567 - false
*/
public class Solution {
    public static void main (String args[]){
        System.out.println(checkTelNumber("(0)501234567"));
    }

    public static boolean checkTelNumber(String telNumber) {
        if (telNumber == null){
            return false;
        }
        String temp = "";
        Pattern p10 = Pattern.compile("^\\d+.+");
        Matcher m10 = p10.matcher(telNumber);
        int format = 0;
        if(telNumber.startsWith("+")){
            temp = telNumber;
            temp = temp.replaceAll("\\(","" );
            temp = temp.replaceAll("\\)","" );
            temp = temp.replaceAll("-","" );
            temp = temp.replaceAll("\\+","" );
            if (temp.length() != 12) return false;
            format = 12;

        }
        else if (telNumber.startsWith("(") || m10.matches()){
            temp = telNumber;
            temp = temp.replaceAll("\\(","" );
            temp = temp.replaceAll("\\)","" );
            temp = temp.replaceAll("-","" );
            if (temp.length() != 10) return false;
            format = 10;
        }
        else return false;
        int count_ = 0;
        int prev_i = -1;
        for (int i = 0; i < telNumber.length(); i++)
        {
            if (telNumber.charAt(i) == '-'){
                count_++;
                if (i>0){
                    if (telNumber.charAt(prev_i) == '-') return false;
                }
            }
            prev_i++;
        }
        if (count_ > 2){
            return false;
        }
        if (format == 12){
            if (telNumber.contains("(")){
                Pattern p12 = Pattern.compile("^\\+\\d+[(]{1}\\d{3}[)]{1}[\\d,-]+\\d+$");
                Matcher m12 = p12.matcher(telNumber);
                if (!m12.matches()) return false;
            }
            else {
                Pattern p12 = Pattern.compile("^\\+\\d+[\\d,-]+\\d+$");
                Matcher m12 = p12.matcher(telNumber);
                if (!m12.matches()) return false;
            }
        }
        if (format == 10){
            if (telNumber.contains("(")){
                p10 = Pattern.compile("^[(]{1}\\d{3}[)]{1}[\\d,-]+\\d+$");
                m10 = p10.matcher(telNumber);
                if (!m10.matches()) return false;
            }
            else {
                p10 = Pattern.compile("^\\d+[\\d,-]+\\d+$");
                m10 = p10.matcher(telNumber);
                if (!m10.matches()) return false;
            }
        }
        return true;
    }
}
