package com.javarush.test.level22.lesson05.task02;

/* Между табуляциями
Метод getPartOfString должен возвращать подстроку между первой и второй табуляцией.
На некорректные данные бросить исключение TooShortStringException.
Класс TooShortStringException не менять.
*/
public class Solution {
    public static String getPartOfString(String string) throws TooShortStringException {
        if (string == null){

            throw new TooShortStringException();
        }
        int index = 0;
        int tabNumber = 0;
        int firstIndex = 0;
        int secondIndex = 0;
        if (string.charAt(0) == '\t'){
            firstIndex = 0;
            tabNumber = 1;
        }
        while(tabNumber < 3 || index != -1){
            index = string.indexOf('\t',index + 1);
            tabNumber++;
            if(tabNumber == 1)firstIndex = index;//first tab index
            if(tabNumber == 2)secondIndex = index;//second tab index

        }
        if(secondIndex <= 0)throw new TooShortStringException();// if amount of tabs is less then 2 then throw an exception
        else{

            if(secondIndex - firstIndex == 1)return "";//if there are two spaces in the row
            return string.substring(++firstIndex,secondIndex);
        }

    }

    public static class TooShortStringException extends Exception {
    }

    public static void main(String[] args) throws TooShortStringException {
        System.out.println(getPartOfString("tab0\ttab\ttab1\t"));       //tab
        System.out.println(getPartOfString("\t\t"));                    //
//        System.out.println(getPartOfString("123\t123"));                //Exception
//        System.out.println(getPartOfString(null));                      //Exception
    }
}
