package com.javarush.test.level20.lesson02.task02;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* Читаем и пишем в файл: JavaRush
Реализуйте логику записи в файл и чтения из файла для класса JavaRush
В файле your_file_name.tmp может быть несколько объектов JavaRush
Метод main реализован только для вас и не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
            File your_file_name = new File("G:\\firstFileName.txt");
            OutputStream outputStream = new FileOutputStream(your_file_name);
            InputStream inputStream = new FileInputStream(your_file_name);

            JavaRush javaRush = new JavaRush();
            User user1 = new User();
            user1.setFirstName("Anatoliy");
            user1.setLastName("Rogovskikh");
            user1.setBirthDate(new Date(89,3,7));
            user1.setCountry(User.Country.RUSSIA);
            user1.setMale(true);
            javaRush.users.add(user1);
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            User user01 = loadedObject.users.get(0);
            System.out.println(user01.getFirstName());
            System.out.println(user01.getLastName());
            System.out.println(user01.getBirthDate());
            System.out.println(user01.isMale());
            System.out.println(user01.getCountry().getDisplayedName());
            //check here that javaRush object equals to loadedObject object - проверьте тут, что javaRush и loadedObject равны

            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception {
            PrintWriter writer = new PrintWriter(outputStream);
            for (User user: users){
                String isUserPresent = user != null ? "yes" : "no";
                writer.println(isUserPresent);
                if (user != null)
                {
                    String isFirstNamePresent = user.getFirstName() != null ? "yes" : "no";
                    writer.println(isFirstNamePresent);
                    if (isFirstNamePresent.equals("yes"))
                    {
                        writer.println(user.getFirstName());
                    }
                    String isLastNamePresent = user.getFirstName() != null ? "yes" : "no";
                    writer.println(isLastNamePresent);
                    if (isLastNamePresent.equals("yes"))
                    {
                        writer.println(user.getLastName());
                    }
                    String isBirthDatePresent = user.getFirstName() != null ? "yes" : "no";
                    writer.println(isBirthDatePresent);
                    if (isBirthDatePresent.equals("yes"))
                    {
                        writer.println(user.getBirthDate().getTime());
                    }
                    String isMalePresent = user.getFirstName() != null ? "yes" : "no";
                    writer.println(isMalePresent);
                    if (isMalePresent.equals("yes"))
                    {
                        writer.println(user.isMale());
                    }
                    String isCountryPresent = user.getFirstName() != null ? "yes" : "no";
                    writer.println(isCountryPresent);
                    if (isCountryPresent.equals("yes"))
                    {
                        writer.println(user.getCountry().getDisplayedName());
                    }
                    writer.flush();
                }
            }
            writer.close();
            //implement this method - реализуйте этот метод
        }

        public void load(InputStream inputStream) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            while (reader.ready())
            {
                String isUserPresent = reader.readLine();
                if (isUserPresent.equals("yes"))
                {
                    User user = new User();
                    String isFirstNamePresent = reader.readLine();
                    if (isFirstNamePresent.equals("yes"))
                    {
                        user.setFirstName(reader.readLine());
                    }
                    String isLastNamePresent = reader.readLine();
                    if (isLastNamePresent.equals("yes"))
                    {
                        user.setLastName(reader.readLine());
                    }
                    String isBirthDatePresent = reader.readLine();
                    if (isBirthDatePresent.equals("yes"))
                    {
                        user.setBirthDate(new Date(Long.parseLong(reader.readLine())));
                    }
                    String isMalePresent = reader.readLine();
                    if (isMalePresent.equals("yes"))
                    {
                        user.setMale(reader.readLine().equals("true"));
                    }
                    String isCountryPresent = reader.readLine();
                    if (isCountryPresent.equals("yes"))
                    {
                        switch (reader.readLine()){
                            case "Ukraine": user.setCountry(User.Country.UKRAINE);
                                break;
                            case "Russia": user.setCountry(User.Country.RUSSIA);
                                break;
                            case "Other": user.setCountry(User.Country.OTHER);
                                break;
                        }
                    }
                    users.add(user);
                }
            }
            reader.close();
            //implement this method - реализуйте этот метод
        }
    }
}
