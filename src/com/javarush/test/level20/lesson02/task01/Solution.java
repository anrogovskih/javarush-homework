package com.javarush.test.level20.lesson02.task01;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* Читаем и пишем в файл: Human
Реализуйте логику записи в файл и чтения из файла для класса Human
Поле name в классе Human не может быть пустым
В файле your_file_name.tmp может быть несколько объектов Human
Метод main реализован только для вас и не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {

            File your_file_name = new File("G:\\firstFileName.txt");
            OutputStream outputStream = new FileOutputStream(your_file_name);
            InputStream inputStream = new FileInputStream(your_file_name);

            Human ivanov = new Human("Ivanov", new Asset("home"), new Asset("car"));
            ivanov.assets.get(0).setPrice(10.0);
            ivanov.assets.get(1).setPrice(11.0);
            ivanov.save(outputStream);
            outputStream.flush();

            Human somePerson = new Human();
            somePerson.load(inputStream);
            System.out.println(somePerson.name);
            System.out.println(somePerson.assets.get(0).getName());
            System.out.println(somePerson.assets.get(0).getPrice());
            System.out.println(somePerson.assets.get(1).getName());
            System.out.println(somePerson.assets.get(1).getPrice());

            //check here that ivanov equals to somePerson - проверьте тут, что ivanov и somePerson равны
            inputStream.close();

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }


    public static class Human {
        public String name;
        public List<Asset> assets = new ArrayList<>();

        public Human() {
        }

        public Human(String name, Asset... assets) {
            this.name = name;
            if (assets != null) {
                this.assets.addAll(Arrays.asList(assets));
            }
        }

        public void save(OutputStream outputStream) throws Exception {
            PrintWriter writer = new PrintWriter(outputStream);
            writer.println(name);
            String doesHeHasAnyAssets = assets.size() != 0 ? "yes": "no";
            writer.println(doesHeHasAnyAssets);
            for (Asset asset: assets){
                writer.println(asset.getName());
                String isPricePresent = asset.getPrice() != 0.0 ? "yes" : "no";
                writer.println(isPricePresent);
                if (isPricePresent.equals("yes"))
                {
                    writer.println(asset.getPrice());
                }
            }
            writer.flush();
            //implement this method - реализуйте этот метод
        }

        public void load(InputStream inputStream) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            name = reader.readLine();
            String doesHeHasAnyAssets = reader.readLine();
            if (doesHeHasAnyAssets.equals("yes")){
                    while (reader.ready())
                    {
                        Asset asset = new Asset(reader.readLine());
                        String isPricePresent = reader.readLine();
                        if (isPricePresent.equals("yes"))
                        {
                            asset.setPrice(Double.parseDouble(reader.readLine()));
                        }
                        assets.add(asset);
                    }
            }
            //implement this method - реализуйте этот метод
        }
    }
}
