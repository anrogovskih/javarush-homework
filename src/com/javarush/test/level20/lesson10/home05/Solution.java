package com.javarush.test.level20.lesson10.home05;

import java.io.*;
import java.util.logging.Logger;

/* Сериализуйте Person
Сериализуйте класс Person стандартным способом. При необходимости поставьте полям модификатор transient.
*/
public class Solution implements Serializable {

    public static class Person implements Serializable{
         String firstName;
         String lastName;
        transient String fullName;
        transient final String greetingString;
        String country;
        Sex sex;
        transient public PrintStream outputStream;
        transient public Logger logger;

        Person(String firstName, String lastName, String country, Sex sex) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.fullName = String.format("%s, %s", lastName, firstName);
            this.greetingString = "Hello, ";
            this.country = country;
            this.sex = sex;
            this.outputStream = System.out;
            this.logger = Logger.getLogger(String.valueOf(Person.class));
        }

        private void writeObject (ObjectOutputStream outputStream)throws IOException{
            outputStream.defaultWriteObject();
            outputStream.writeObject(fullName);
            outputStream.writeObject(country);
            outputStream.writeObject(sex);
        }
        private void readObject (ObjectInputStream inputStream)throws IOException, ClassNotFoundException{
            inputStream.defaultReadObject();
            fullName = (String) inputStream.readObject();
            country = (String) inputStream.readObject();
            sex = (Sex) inputStream.readObject();
            String[] temp = fullName.split(",");
            lastName = temp[0];
            firstName = temp[1].substring(1);
            this.logger = Logger.getLogger(String.valueOf(Person.class));
            this.outputStream = System.out;

        }

    }

    enum Sex {
        MALE,
        FEMALE
    }
    public static void main (String [] args) throws IOException, ClassNotFoundException
    {
//        Solution solution = new Solution();
//        Solution.Person person = solution.new Person("Anatoliy","Rogovskikh","Russia",Sex.MALE);
//        FileOutputStream fileOutput = new FileOutputStream("your.file.name");
//        ObjectOutputStream outputStream = new ObjectOutputStream(fileOutput);
//        outputStream.writeObject(person);
//        System.out.println(person.outputStream);
//        fileOutput.close();
//        outputStream.close();
//        FileInputStream fiStream = new FileInputStream("your.file.name");
//        ObjectInputStream objectStream = new ObjectInputStream(fiStream);
//        Person loadedPerson = (Person) objectStream.readObject();
//        System.out.println(loadedPerson.firstName);
//        System.out.println(loadedPerson.lastName);
//        System.out.println(loadedPerson.country);
//        System.out.println(loadedPerson.sex);
//        System.out.println(loadedPerson.fullName);
//        System.out.println(loadedPerson.greetingString);
//        System.out.println(loadedPerson.logger);
//        System.out.println(loadedPerson.outputStream);
    }
}
