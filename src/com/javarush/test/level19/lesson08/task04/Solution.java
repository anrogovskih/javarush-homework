package com.javarush.test.level19.lesson08.task04;

/* Решаем пример
В методе main подмените объект System.out написанной вами ридер-оберткой по аналогии с лекцией
Ваша ридер-обертка должна выводить на консоль решенный пример
Вызовите готовый метод printSomething(), воспользуйтесь testString
Верните переменной System.out первоначальный поток

Возможные операции: + - *
Шаблон входных данных и вывода: a [знак] b = c
Отрицательных и дробных чисел, унарных операторов - нет.

Пример вывода:
3 + 6 = 9
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream consoleStream = System.out;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream newStream = new PrintStream(outputStream);
        System.setOut(newStream);
        testString.printSomething();
        System.setOut(consoleStream);
        String [] array = outputStream.toString().split(" ");
        StringBuilder sb = new StringBuilder();
        int a = 0;
        int b = 0;
        char operator = 0;
        int count = 0;
        for (int i = 0; i < array.length; i++)
        {
            if (Character.isDigit(array[i].charAt(0)))
                {
                    for (int j = 0; j <array[i].length(); j++)
                    {
                        sb.append(array[i].charAt(j));
                    }
                    if (count == 0)
                    {
                        a = Integer.parseInt(sb.toString());
                        count++;
                        sb = new StringBuilder();
                    }
                    else b = Integer.parseInt(sb.toString());
                }
            else if (array[i].equals("+")){
                    operator = '+';
                }
            else if (array[i].equals("-")){
                    operator = '-';
                }
            else if (array[i].equals("*")){
                operator = '*';
            }
        }
        int result;
        if (operator == '+'){
            result = a+b;
        }
        else if (operator == '-'){
            result = a-b;}
        else result = a*b;
        System.out.print(a);
        System.out.print(" ");
        System.out.print(operator);
        System.out.print(" ");
        System.out.print(b);
        System.out.print(" ");
        System.out.print('=');
        System.out.print(" ");
        System.out.print(result);

    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}

