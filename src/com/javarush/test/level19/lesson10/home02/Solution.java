package com.javarush.test.level19.lesson10.home02;

/* Самый богатый
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Вывести в консоль имена, у которых максимальная сумма
Имена разделять пробелом либо выводить с новой строки
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Петров 0.501
Иванов 1.35
Петров 0.85

Пример вывода:
Петров
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        String s = reader.readLine();
        ArrayList<String> list = new ArrayList<>();
        Map<String, Double> resultMap = new TreeMap<>();
        while (s != null){
            list.add(s);
            s = reader.readLine();
        }
        reader.close();
        for (int i = 0; i < list.size(); i++)
        {
            String [] temp = list.get(i).split(" ");
            if (resultMap.containsKey(temp[0])){
                resultMap.put(temp[0], Double.parseDouble(temp[1]) + resultMap.get(temp[0]));
            }
            else resultMap.put(temp[0], Double.parseDouble(temp[1]));
        }
        double max = Double.MIN_VALUE;
        String theReachest = "Bill Gates";
        for (Map.Entry<String, Double> pair: resultMap.entrySet()){
            if(pair.getValue()> max){
             max = pair.getValue();
                theReachest = pair.getKey();
            }
        }
        System.out.println(theReachest);
    }
}
