package com.javarush.test.level19.lesson10.home07;

/* Длинные слова
В метод main первым параметром приходит имя файла1, вторым - файла2
Файл1 содержит слова, разделенные пробелом.
Записать через запятую в Файл2 слова, длина которых строго больше 6
Закрыть потоки. Не использовать try-with-resources

Пример выходных данных:
длинное,короткое,аббревиатура
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        String s = reader.readLine();
        BufferedWriter writer = new BufferedWriter(new FileWriter(args[1]));
        boolean isFirstWriting = true;
        while (s != null){
            String [] array = s.split(" ");
            for (String str: array){
                if (str.length() >6){
                    if (!isFirstWriting){
                        writer.write(',');
                    }
                    writer.write(str);
                    isFirstWriting = false;
            }

        }
            s = reader.readLine();
    }
        reader.close();
        writer.close();
    }
}
