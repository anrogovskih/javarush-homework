package com.javarush.test.level19.lesson10.home01;

/* ������� ��������
� ����� main ������ ���������� �������� ��� �����.
� ���� ����� ������ ������ ����� ��������� ���:
��� ��������
��� [���] - String, [��������] - double. [���] � [��������] ��������� ��������

��� ������� ����� ��������� ����� ���� ��� ��������
��� ������ ������� � �������, �������������� ������������ � ������������ ������� �� �����
������� ������

������ �������� �����:
������ 2
������� 6
������ 1.35
������ 3.1

������ ������:
������ 1.35
������ 5.1
������� 6.0
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        String s = reader.readLine();
        ArrayList <String> list = new ArrayList<>();
        Map <String, Double> resultMap = new TreeMap<>();
        while (s != null){
            list.add(s);
            s = reader.readLine();
        }
        reader.close();
        for (int i = 0; i < list.size(); i++)
        {
            String [] temp = list.get(i).split(" ");
            if (resultMap.containsKey(temp[0])){
                resultMap.put(temp[0], Double.parseDouble(temp[1]) + resultMap.get(temp[0]));
            }
            else resultMap.put(temp[0], Double.parseDouble(temp[1]));
        }
        for (Map.Entry<String, Double> pair: resultMap.entrySet()){
            System.out.println(pair.getKey() + " " + pair.getValue());
        }
    }
}
