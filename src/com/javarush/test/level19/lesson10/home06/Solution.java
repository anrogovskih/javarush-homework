package com.javarush.test.level19.lesson10.home06;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


/* Замена чисел
1. В статическом блоке инициализировать словарь map парами [число-слово] от 0 до 12 включительно
Например, 0 - "ноль", 1 - "один", 2 - "два"
2. Считать с консоли имя файла
3. Заменить все числа на слова используя словарь map
4. Результат вывести на экран
5. Закрыть потоки. Не использовать try-with-resources

Пример данных:
Это стоит 1 бакс, а вот это - 12 .
Переменная имеет имя file1.
110 - это число.

Пример вывода:
Это стоит один бакс, а вот это - двенадцать .
Переменная имеет имя file1.
110 - это число.
*/

public class Solution {
    public static Map<Integer, String> map = new HashMap<Integer, String>();
    static {
        map.put(0, "ноль");
        map.put(1, "один");
        map.put(2, "два");
        map.put(3, "три");
        map.put(4, "четыре");
        map.put(5, "пять");
        map.put(6, "шесть");
        map.put(7, "семь");
        map.put(8, "восемь");
        map.put(9, "девять");
        map.put(10, "десять");
        map.put(11, "одиннадцать");
        map.put(12, "двенадцать");
    }

    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
        String s;
        while ((s=fileReader.readLine())!=null)
        {

            for (String o: s.split(" "))
            {
                for (Integer integer : map.keySet())
                {
                    try
                    {
                        int x = Integer.parseInt(o);
                        if (integer == x) s = s.replaceFirst(String.valueOf(integer),map.get(integer));
                    }
                    catch (NumberFormatException e){

                    }
//                if (isStringDigit(o)){
//                    s = iteration(o,s);
//                }
//                else if ((isStringDigit(o)) && (o.charAt(0) == '0')){
//                iteration(String.valueOf(Integer.parseInt(o)),s);
//                }
//                else if ((o.length() == 1)){
//                    s = iteration(o,s);
//                }
//                else if ((o.length() == 2)){
//                    if (Character.isDigit(o.charAt(1))){
//                        s = iteration(o,s);
//                    }
//                    else if (!(Character.isLetter(o.charAt(0))) && Character.isDigit(o.charAt(1)))
//                    {
//                        s = iteration(o.substring(1), s);
//                    }
//                    else if (!(Character.isLetter(o.charAt(1)))){
//                        s = iteration(o.substring(0,1),s);
//                    }
//
//                }
//                else if ((o.length() == 3)){
//                    if (isStringDigit(o))
//                    {
//                        s = iteration(o,s);
//                    }
//                    else if (!(Character.isLetter(o.charAt(2))) && !(Character.isDigit(o.charAt(2)))){
//                        s = iteration(o.substring(0,2),s);
//                    }
//                }


                }






            }

            System.out.println(s);
        }
        reader.close();
        fileReader.close();
    }
    private static String iteration (String o, String s){
        for (Integer integer : map.keySet())
        {
            String str = integer+"";

            if (str.equals(o)) s = s.replaceFirst(str,map.get(integer));

        }
        return s;
    }
    private static boolean isStringDigit (String s){
        for (int i = 0; i < s.length(); i++)
        {
            if (!Character.isDigit(s.charAt(i))){
                return false;
            }
        }
        return true;
    }
}

