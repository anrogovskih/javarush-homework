package com.javarush.test.level19.lesson10.home03;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/* Хуан Хуанович
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя день месяц год
где [имя] - может состоять из нескольких слов, разделенных пробелами, и имеет тип String
[день] - int, [месяц] - int, [год] - int
данные разделены пробелами

Заполнить список PEOPLE импользуя данные из файла
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Иванов Иван Иванович 31 12 1987
Вася 15 5 2013
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        String s = reader.readLine();
        ArrayList<String> list = new ArrayList<>();
        Map<String, Double> resultMap = new TreeMap<>();
        while (s != null){
            list.add(s);
            s = reader.readLine();
        }
        reader.close();
        int timePeriod = 1;
        int day=0;
        int month=0;
        int year=0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++)
        {
            String [] temp = list.get(i).split(" ");
            for (int j = 0; j < temp.length; j++)
            {
                    if (Character.isDigit(temp[j].charAt(0))){
                        switch (timePeriod){
                            case 1:
                                day = Integer.parseInt(temp[j]);
                                timePeriod = 2;
                                break;
                            case 2:
                                month = Integer.parseInt(temp[j])-1;
                                timePeriod = 3;
                                break;
                            case 3:
                                year = Integer.parseInt(temp[j]);
                                timePeriod = 1;
                                break;
                        }
                    }
                else {
                        sb.append(" ");
                        sb.append(temp[j]);
                    }

            }
            PEOPLE.add(new Person(sb.toString().substring(1),new Date(year,month,day)));
            sb = new StringBuilder();
        }
//        for (Person p: PEOPLE){
//            System.out.println(p.getName());
//            System.out.println(p.getBirthday());
//        }
    }


}
