package com.javarush.test.level19.lesson03.task05;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/* Закрепляем адаптер
Адаптировать Customer и Contact к RowItem.
Классом-адаптером является DataAdapter.
Инициализируйте countries перед началом выполнения программы. Соответствие кода страны и названия:
UA Ukraine
RU Russia
CA Canada
*/

public class Solution {
    private static Map<String,String> countries = new HashMap<String,String>();
    static {
        countries.put("Ukraine", "UA");
        countries.put("Russia", "RU");
        countries.put("Canada", "CA");
    }
    public static class DataAdapter implements RowItem {
        private Customer customer;
        private Contact contact;
        public DataAdapter(Customer customer, Contact contact) {
            this.customer = customer;
            this.contact = contact;
        }

        @Override
        public String getCountryCode()
        {
            return countries.get(this.customer.getCountryName());
        }

        @Override
        public String getCompany()
        {
            return this.customer.getCompanyName();
        }

        @Override
        public String getContactFirstName()
        {
            String[] array = this.contact.getName().split(" ");
            return array[1];
        }

        @Override
        public String getContactLastName()
        {
            String[] array = this.contact.getName().split(" ");
            return array[0].substring(0,array[0].length()-1);
        }

        @Override
        public String getDialString()
        {
            char [] array = this.contact.getPhoneNumber().toCharArray();
            ArrayList <Character> list = new ArrayList<>();
            for (int i = 0; i < array.length; i++)
            {
             if (Character.isDigit(array[i])){
                 list.add(array[i]);
             }
            }
            String result = "callto://+";
            for (int i = 0; i < list.size(); i++)
            {
                result = result.concat(list.get(i).toString());
            }
            return result;
        }
    }

    public static interface RowItem {
        String getCountryCode();        //example UA
        String getCompany();            //example JavaRush Ltd.
        String getContactFirstName();   //example Ivan
        String getContactLastName();    //example Ivanov
        String getDialString();         //example callto://+380501234567
    }

    public static interface Customer {
        String getCompanyName();        //example JavaRush Ltd.
        String getCountryName();        //example Ukraine
    }

    public static interface Contact {
        String getName();               //example Ivanov, Ivan
        String getPhoneNumber();        //example +38(050)123-45-67
    }
}
class Test{
    public static void main(String[] args){
        Solution.Customer customer = new Solution.Customer()
        {
            @Override
            public String getCompanyName()
            {
                return "JavaRush Ltd.";
            }

            @Override
            public String getCountryName()
            {
                return "Ukraine";
            }
        };
        Solution.Contact contact = new Solution.Contact()
        {
            @Override
            public String getName()
            {
                return "Ivanov, Ivan";
            }

            @Override
            public String getPhoneNumber()
            {
                return "+38(050)123-45-67";
            }
        };
        Solution.DataAdapter adapter = new Solution.DataAdapter(customer, contact);
        System.out.println(adapter.getCompany());
        System.out.println(adapter.getContactFirstName());
        System.out.println(adapter.getContactLastName());
        System.out.println(adapter.getCountryCode());
        System.out.println(adapter.getDialString());
        }
    }
