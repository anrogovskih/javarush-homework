package com.javarush.test.level19.lesson03.task03;

/* Адаптация нескольких интерфейсов
Адаптировать IncomeData к Customer и Contact.
Классом-адаптером является IncomeDataAdapter.
Инициализируйте countries перед началом выполнения программы. Соответствие кода страны и названия:
UA Ukraine
RU Russia
CA Canada
Дополнить телефонный номер нулями до 10 цифр при необходимости (смотри примеры)
Обратите внимание на формат вывода фамилии и имени человека
*/

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static Map<String, String> countries = new HashMap<String, String>();
    static {
        countries.put("UA", "Ukraine");
        countries.put("RU", "Russia");
        countries.put("CA", "Canada");
    }

    public static class IncomeDataAdapter implements Customer, Contact{
        private IncomeData incomeData;
        IncomeDataAdapter (IncomeData incomeData){
            this.incomeData = incomeData;
        }
        @Override
        public String getName()
        {
            return  this.incomeData.getContactLastName()+ ", "+ this.incomeData.getContactFirstName();
        }

        @Override
        public String getPhoneNumber()
        {
            StringBuilder sb = new StringBuilder();
            sb.append('+');
            sb.append(this.incomeData.getCountryPhoneCode());
            String temp = Integer.toString(this.incomeData.getPhoneNumber());
            if (temp.length() < 10){
                while (temp.length()<10){
                    temp = '0' + temp;
                }
            }
            for (int i = 0; i < temp.length(); i++)
            {
                if (i == 0){
                    sb.append('(');
                }
                if (i == 3){
                    sb.append(')');
                }
                if (i == 6 || i == 8){
                    sb.append('-');
                }
                sb.append(temp.charAt(i));
            }
            return sb.toString();
        }

        @Override
        public String getCompanyName()
        {
            return this.incomeData.getCompany();
        }

        @Override
        public String getCountryName()
        {
            return countries.get(this.incomeData.getCountryCode());
        }
    }

    public static interface IncomeData {
        String getCountryCode();        //example UA

        String getCompany();            //example JavaRush Ltd.

        String getContactFirstName();   //example Ivan

        String getContactLastName();    //example Ivanov

        int getCountryPhoneCode();      //example 38

        int getPhoneNumber();           //example 501234567
    }

    public static interface Customer {
        String getCompanyName();        //example JavaRush Ltd.

        String getCountryName();        //example Ukraine
    }

    public static interface Contact {
        String getName();               //example Ivanov, Ivan

        String getPhoneNumber();        //example +38(050)123-45-67
    }
    public static class Test{
        public static void main (String args []){
            IncomeData data = new IncomeData()
            {
                @Override
                public String getCountryCode()
                {
                    return "UA";
                }

                @Override
                public String getCompany()
                {
                    return "JavaRush Ltd.";
                }

                @Override
                public String getContactFirstName()
                {
                    return "Ivan";
                }

                @Override
                public String getContactLastName()
                {
                    return "Ivanov";
                }

                @Override
                public int getCountryPhoneCode()
                {
                    return 38;
                }

                @Override
                public int getPhoneNumber()
                {
                    return 501234567;
                }
            };
            IncomeDataAdapter adapter = new IncomeDataAdapter(data);
            System.out.println(adapter.getCompanyName());
            System.out.println(adapter.getCountryName());
            System.out.println(adapter.getName());
            System.out.println(adapter.getPhoneNumber());

        }
    }
}