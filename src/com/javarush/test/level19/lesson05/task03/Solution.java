package com.javarush.test.level19.lesson05.task03;

/* Выделяем числа
Считать с консоли 2 имени файла.
Вывести во второй файл все числа, которые есть в первом файле.
Числа выводить через пробел.
Закрыть потоки. Не использовать try-with-resources

Пример тела файла:
12 text var2 14 8v 1

Результат:
12 14 1
*/

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
        FileReader fr = new FileReader(fileName1);
        FileWriter fw = new FileWriter(fileName2);
        reader.close();
        StringBuilder sb = new StringBuilder();
        ArrayList<String> list = new ArrayList<>();
        while (fr.ready()){
            int data = fr.read();
            if (((char)data)==32){
                list.add(sb.toString());
                sb = new StringBuilder();
            }
            else {
                sb.append((char) data);
            }
        }
        list.add(sb.toString());
        fr.close();
        Pattern p = Pattern.compile("[0-9]+");
        Matcher m;
        for (int i = 0; i < list.size(); i++)
        {
            m = p.matcher(list.get(i));
            if (m.matches()){
                char [] temp = list.get(i).toCharArray();
                for (int j = 0; j < temp.length; j++)
                {
                    fw.write((int) temp[j]);
                }
                fw.write(' ');
            }
        }
        fw.close();
    }
}
