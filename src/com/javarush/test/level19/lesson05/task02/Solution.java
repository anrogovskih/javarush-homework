package com.javarush.test.level19.lesson05.task02;

/* Считаем слово
Считать с консоли имя файла.
Файл содержит слова, разделенные знаками препинания.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        FileReader fr = new FileReader(fileName);
        reader.close();
        StringBuilder sb = new StringBuilder();
        ArrayList <String> list = new ArrayList<>();
        while (fr.ready()){
            int data = fr.read();
            if ((data <91 && data >64) ||(data <123 && data >96)){
                sb.append((char) data);
            }
            else {
                list.add(sb.toString());
                sb = new StringBuilder();
            }
        }
        fr.close();
        int count = 0;
        for (int i = 0; i < list.size(); i++)
        {
            if (list.get(i).equals("world")){
                count++;
            }
        }
        System.out.println(count);
    }
}
