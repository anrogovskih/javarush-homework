package com.javarush.test.level19.lesson05.task01;

/* Четные байты
Считать с консоли 2 имени файла.
Вывести во второй файл все байты с четным индексом.
Пример: второй байт, четвертый байт, шестой байт и т.д.
Закрыть потоки ввода-вывода.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        String s1 = reader.readLine();
        FileReader fReader = new FileReader(s);
        FileWriter writer = new FileWriter(s1);
        boolean isEven = false;
        while (fReader.ready()){
            int data = fReader.read();
            if (isEven)
            {
                writer.write(data);
                isEven = false;
            }
            else isEven = true;
        }
        fReader.close();
        writer.close();
    }
}
