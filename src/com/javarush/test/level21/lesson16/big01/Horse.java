package com.javarush.test.level21.lesson16.big01;



/**
 * Created by Анатолий on 26.06.2016.
 */
public class Horse
{
    public String name;
    public double speed;
    public double distance;

    public void move(){
        this.distance += this.speed* Math.random();
    }
    public void print(){
        String distanceString = "";
        for (int i = 0; i < Math.round(this.distance); i++)
        {
            distanceString +=".";
        }
        System.out.println(distanceString + this.getName());
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setSpeed(double speed)
    {
        this.speed = speed;
    }

    public void setDistance(double distance)
    {
        this.distance = distance;
    }

    public String getName()
    {

        return name;
    }

    public double getSpeed()
    {
        return speed;
    }

    public double getDistance()
    {
        return distance;
    }

    public Horse(String name, double speed, double distance)
    {
        this.name = name;
        this.speed = speed;
        this.distance = distance;
    }
}
