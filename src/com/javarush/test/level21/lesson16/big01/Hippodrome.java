package com.javarush.test.level21.lesson16.big01;

import java.util.ArrayList;

/**
 * Created by Анатолий on 26.06.2016.
 */
public class Hippodrome
{


    public static ArrayList<Horse> horses = new ArrayList<Horse>();
    public static Hippodrome game;
    public ArrayList<Horse> getHorses()
    {
        return horses;
    }

    public void move(){
        for (Horse horse : getHorses())
        {
            horse.move();
        }
    }
    public void print(){
        for (Horse horse : getHorses())
        {
            horse.print();
        }
        System.out.println();
        System.out.println();
    }
    public void run() throws InterruptedException
    {
        for (int i = 0; i < 100; i++)
        {
            move();
            print();
            Thread.sleep(200);
        }
    }
    public Horse getWinner(){
        double max = 0;
        Horse winner = null;
        for (Horse horse : getHorses())
        {
            if (horse.getDistance() > max){
                winner = horse;
                max = horse.getDistance();
            }
        }
        return winner;
    }
    public void printWinner(){
        System.out.println("Winner is " + getWinner().getName() + "!");

    }

    public void setHorses(ArrayList<Horse> horses)
    {
        this.horses = horses;
    }

    public static void main (String args[]){
        game = new Hippodrome();
        Horse lightning = new Horse("Lightning",3,0);
        Horse victory = new Horse("Victory",3,0);
        Horse mrBlood = new Horse("mr. Blood",3,0);

        ArrayList<Horse> horses1 = new ArrayList<Horse>();
        horses1.add(lightning);
        horses1.add(victory);
        horses1.add(mrBlood);
        game.setHorses(horses1);
        try
        {
            game.run();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        game.printWinner();
    }
}
