package com.javarush.test.level21.lesson08.task03;

/* Запретить клонирование
Разрешите клонировать класс А
Запретите клонировать класс B
Разрешите клонировать класс C
Метод main не участвует в тестировании.
*/
public class Solution {
    public static class A implements Cloneable {
        private int i;
        private int j;

        @Override
        protected A clone() throws CloneNotSupportedException
        {
            A a = new A(this.getI(), this.getJ());
            return a;
        }

        public A(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }
    }

    public static class B extends A {
        private String name;

        public B(int i, int j, String name) {
            super(i, j);
            this.name = name;
        }

        public String getName() {
            return name;
        }
        @Override
        protected B clone() throws CloneNotSupportedException
        {
            throw new CloneNotSupportedException();
        }
    }

    public static class C extends B implements Cloneable {
        public C(int i, int j, String name) {
            super(i, j, name);
        }
        @Override
        protected C clone() throws CloneNotSupportedException
        {
            return new C(this.getI(), this.getJ(), this.getName());
        }
    }



    public static void main(String[] args) {
        A a = new A(1,2);
        A cloneA = null;
        try
        {
            cloneA= a.clone();
        }
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }
//        System.out.println(a);
//        System.out.println(cloneA);
//
//        System.out.println(a.getI());
//        System.out.println(cloneA.getI());

        C c = new C(2,3,"Love");
        C cloneC = null;
        try
        {
            cloneC = c.clone();
        }
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }
        System.out.println(c);
        System.out.println(cloneC);

        System.out.println(c.getI());
        System.out.println(c.getJ());
        System.out.println(c.getName());

        System.out.println(cloneC.getI());
        System.out.println(cloneC.getJ());
        System.out.println(cloneC.getName());
    }
}
