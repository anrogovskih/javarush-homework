package com.javarush.test.level21.lesson05.task03;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/* Ошибка в equals/hashCode
Исправьте ошибки реализаций методов equals и hashCode для класса Solution
*/
public class Solution {
    private int anInt;
    private String string;
    private double aDouble;
    private Date date;
    private Solution solution;

    public Solution(int anInt, String string, double aDouble, Date date, Solution solution) {
        this.anInt = anInt;
        this.string = string;
        this.aDouble = aDouble;
        this.date = date;
        this.solution = solution;
    }
    public Solution(){}

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Solution)) return false;

        Solution solution1 = (Solution) o;

        if (Double.compare(solution1.aDouble, aDouble) != 0) return false;
        if (anInt != solution1.anInt) return false;
        if (date != null && solution1.date != null){
            if (!date.equals(solution1.date)) {
                return false;
            }
        }
        else if(date != null || solution1.date != null){
            return false;
        }
        if (solution != null && solution1.solution != null){
            if (!solution.equals(solution1.solution)) {
                return false;
            }
        }
        else if(solution != null || solution1.solution != null){
            return false;
        }
        if (string != null && solution1.string != null){
            if (!string.equals(solution1.string)) {
                return false;
            }
        }
        else if(string != null || solution1.string != null){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = anInt;
        temp = aDouble != 0.0d ? Double.doubleToLongBits(aDouble) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (date != null ? date.hashCode() : new Date().hashCode());
        result = 31 * result + (solution != null ? solution.hashCode() : 0);
        result = 31 * result + (string != null ? string.hashCode() : 0);
        return result;
    }
    public static void main(String[] args)
    {
        Set<Solution> s = new HashSet<>();
        Solution solution = new Solution(7, "Donald", 7.7, new Date(3544561655L), new Solution());
//        s.add(new Solution("Donald", "Duck"));
//        System.out.println(new Solution(null, null).equals(new Solution(null, null)));
//        System.out.println(new Solution("first", null).equals(new Solution("first", null)));
//        System.out.println(new Solution(null, "last").equals(new Solution(null, "last")));
        System.out.println(solution.equals(new Solution(7, "Donald", 7.7, new Date(3544561655L), new Solution())));
        System.out.println(new Date());
//        System.out.println(s.contains(new Solution("Donald", "Duck")));
    }
}
