package com.javarush.test.level21.lesson05.task01;

import java.util.HashSet;
import java.util.Set;

/* Equals and HashCode
В классе Solution исправить пару методов equals/hashCode в соответствии с правилами реализации этих методов.
Метод main не участвует в тестировании.
*/
public class Solution {
    private final String first, last;

    public Solution(String first, String last) {
        this.first = first;
        this.last = last;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Solution solution = (Solution) o;
        if ((first != null && solution.first != null) && (last != null && solution.last != null))
        {
            if (!first.equals(solution.first)) return false;
            return last.equals(solution.last);
        }
        else if (first == null && solution.first == null){
            if (last == null && solution.last == null){
                return true;
            }
            if (last == null || solution.last == null){
                return false;
            }
            return last.equals(solution.last);
        }
        else if (first == null || solution.first == null){
            return false;
        }
        else if (!first.equals(solution.first)) return false;

        else if (last == null && solution.last == null){
            return true;
        }
        else return false;

    }

    @Override
    public int hashCode()
    {
        int result;
        if (first == null){
            result = 0;
        }
        else
        {
            result = first.hashCode();
        }
        if (last != null)
        {
            result = 31 * result + last.hashCode();
        }
        else result = 31 * result + 0;
        return result;
    }


    public static void main(String[] args) {
        Set<Solution> s = new HashSet<>();
        Solution solution = new Solution("Donald", "Duck");
        s.add(new Solution("Donald", "Duck"));
        System.out.println(new Solution(null, null).equals(new Solution(null, null)));
        System.out.println(new Solution("first", null).equals(new Solution("first", null)));
        System.out.println(new Solution(null, "last").equals(new Solution(null, "last")));
        System.out.println(solution.equals(new Solution("Donald", "Duck")));
        System.out.println(s.contains(new Solution("Donald", "Duck")));
        //    public boolean equals(Object obj) {
//        if (obj == null){
//            return false;
//        }
//        if (obj.getClass() != this.getClass()){
//            return false;
//        }
//        Solution n = (Solution) obj;
//        return n.first.equals(first) && n.last.equals(last);
//    }
//
//    public int hashCode() {
//        return 31*first.hashCode() + last.hashCode();
//    }
    }
}
