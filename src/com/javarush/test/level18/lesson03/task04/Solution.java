package com.javarush.test.level18.lesson03.task04;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* Самые редкие байты
Ввести с консоли имя файла
Найти байт или байты с минимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputStream = new FileInputStream(reader.readLine());
        Map<Integer, Integer> map = new HashMap<>();
        while (inputStream.available() > 0)
        {
            int data = inputStream.read();
            if (map.containsKey(data))
            {
                map.put(data, map.get(data) + 1);
            } else
            {
                map.put(data, 1);
            }
        }
        inputStream.close();
        ArrayList <Integer> list = new ArrayList<Integer>();
        int min = Integer.MAX_VALUE;
        for (Map.Entry<Integer, Integer> entry : map.entrySet())
        {
            if (entry.getValue() < min)
            {
                min = entry.getValue();
                list.clear();
                list.add(entry.getKey());
            }
            else if (entry.getValue() == min){
                list.add(entry.getKey());
            }
        }

        ArrayList<Integer> list2 = new ArrayList<>();
        list.forEach(val -> System.out.print(val + " "));
        System.out.println();
        list.forEach(val -> list2.add(val * 2));
        list2.forEach(val -> System.out.print(val + " "));
    }
}
