package com.javarush.test.level18.lesson03.task03;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* Самые частые байты
Ввести с консоли имя файла
Найти байт или байты с максимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputStream = new FileInputStream(reader.readLine());
        Map<Integer, Integer> bytes = new TreeMap<>();
        while (inputStream.available() > 0){
            int data = inputStream.read();
            if (bytes.containsKey(data)){
                bytes.put(data,bytes.get(data)+1);
            }
            else {bytes.put(data, 1);}
        }

        int max = 0;

        for (Map.Entry<Integer, Integer> pair: bytes.entrySet())
        {
            System.out.println(pair);
        }

        Iterator iterator = bytes.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry pair = (Map.Entry)iterator.next();
            if ((int) pair.getValue() > max){
                max = (int) pair.getValue();
            }
        }
        iterator = bytes.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry pair = (Map.Entry)iterator.next();
            if ((int) pair.getValue() == max){
                System.out.println((int) pair.getKey()+" ");
            }
        }
    }
}
