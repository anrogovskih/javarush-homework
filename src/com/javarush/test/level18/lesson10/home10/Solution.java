package com.javarush.test.level18.lesson10.home10;

/* Собираем файл
Собираем файл из кусочков
Считывать с консоли имена файлов
Каждый файл имеет имя: [someName].partN. Например, Lion.avi.part1, Lion.avi.part2, ..., Lion.avi.part37.
Имена файлов подаются в произвольном порядке. Ввод заканчивается словом "end"
В папке, где находятся все прочтенные файлы, создать файл без приставки [.partN]. Например, Lion.avi
В него переписать все байты из файлов-частей используя буфер.
Файлы переписывать в строгой последовательности, сначала первую часть, потом вторую, ..., в конце - последнюю.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Map<Integer, String> map = new TreeMap<>();
        Pattern p1 = Pattern.compile("\\.[a-z]+$");
        String temp = "";
        while (true)
        {
            String filename = reader.readLine();
            if (filename.equals("end"))
            {
                break;
            }
            Matcher m1 = p1.matcher(filename);
            if (m1.find())
            {
                int key = Integer.parseInt(filename.substring(filename.lastIndexOf(".part") + 5, m1.start()));
                map.put(key, filename);
            }
            temp = filename;
        }
        reader.close();
        Matcher m = p1.matcher(temp);
        m.find();
        String commonFilePath = temp.substring(0,temp.lastIndexOf(".part"));
        commonFilePath = commonFilePath.concat(temp.substring(m.start()));
        FileOutputStream outputStream = new FileOutputStream(commonFilePath);
        for (Map.Entry<Integer, String> pair : map.entrySet())
        {
            FileInputStream inputStream = new FileInputStream(pair.getValue());
            byte[] buffer = new byte[inputStream.available()];
            while (inputStream.available() > 0)
            {
                int data = inputStream.read(buffer);
                outputStream.write(buffer, 0, data);
            }
            inputStream.close();
        }

        outputStream.close();

    }
}

