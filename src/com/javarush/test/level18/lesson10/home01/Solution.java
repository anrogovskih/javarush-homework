package com.javarush.test.level18.lesson10.home01;

/* Английские буквы
В метод main первым параметром приходит имя файла.
Посчитать количество букв английского алфавита, которое есть в этом файле.
Вывести на экран число (количество букв)
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new FileReader(args [0]));
            String s;
            ArrayList <Character> charList = new ArrayList<>();
            while ((s = br.readLine()) != null){
                char [] temp = s.toCharArray();
                for (int i = 0; i < temp.length; i++)
                {
                    charList.add(temp [i]);
                }
        }
        br.close();
        int count = 0;
        for (int i = 0; i < charList.size(); i++)
        {
            if (((charList.get(i) > 64) && (charList.get(i) < 91)) || ((charList.get(i) > 96) && (charList.get(i) < 123))){
                count++;
            }
        }
        System.out.println(count);

    }
}
