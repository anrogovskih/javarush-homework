package com.javarush.test.level18.lesson10.home07;

/* Поиск данных внутри файла
Считать с консоли имя файла
Найти в файле информацию, которая относится к заданному id, и вывести ее на экран в виде, в котором она записана в файле.
Программа запускается с одним параметром: id (int)
Закрыть потоки. Не использовать try-with-resources

В файле данные разделены пробелом и хранятся в следующей последовательности:
id productName price quantity

где id - int
productName - название товара, может содержать пробелы, String
price - цена, double
quantity - количество, int

Информация по каждому товару хранится в отдельной строке
*/

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader fileReader = new BufferedReader(new FileReader(reader.readLine()));
        String temp;
        while ( (temp = fileReader.readLine()) != null)
        {
            if (Integer.parseInt(temp.substring(0, temp.indexOf(" "))) == Integer.parseInt(args[0])){
                System.out.println(temp);
            }
        }
        reader.close();
        fileReader.close();
//        FileInputStream inputStream = new FileInputStream(reader.readLine());
//        ArrayList <Character> characters = new ArrayList<>();
//        while (inputStream.available()>0){
//            int data = inputStream.read();
//            characters.add((char) data);
//        }
//        StringBuilder sb = new StringBuilder();
//        ArrayList <String> strings = new ArrayList<>();
//        for (int i = 0; i < characters.size(); i++)
//        {
//            if (characters.get(i)==10){
//                strings.add(sb.toString());
//                sb = new StringBuilder();
//                i++;
//            }
//            sb.append(characters.get(i));
//        }
//        strings.add(sb.toString());
//        reader.close();
//        inputStream.close();
//        Map <Integer, String> productDescriptionMap = new HashMap<>();
//        String[] temp;
//        String temp1 = "";
//        for (String elem:strings){
//            temp = elem.split(" ");
//            productDescriptionMap.put(Integer.parseInt(temp[0]), elem);
//        }
//        System.out.print(productDescriptionMap.get(Integer.parseInt(args[0])));
    }
}

