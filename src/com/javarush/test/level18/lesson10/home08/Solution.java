package com.javarush.test.level18.lesson10.home08;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/* Нити и байты
Читайте с консоли имена файлов, пока не будет введено слово "exit"
Передайте имя файла в нить ReadThread
Нить ReadThread должна найти байт, который встречается в файле максимальное число раз, и добавить его в словарь resultMap,
где параметр String - это имя файла, параметр Integer - это искомый байт.
Закрыть потоки. Не использовать try-with-resources
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true){
            String filename = reader.readLine();
            if (filename.equals("exit")){
                break;
            }
            ReadThread thread = new ReadThread(filename);
            thread.start();
        }
    }

    public static class ReadThread extends Thread {
        String filename;
        public ReadThread(String fileName) {
            this.filename = fileName;
            //implement constructor body
        }
        public void run(){
            Map <Integer, Integer> temp = new HashMap<>();
            try
            {
                FileInputStream inputStream = new FileInputStream(filename);
                while (inputStream.available() > 0){
                    int data = inputStream.read();
                    if (temp.containsKey(data)){
                        temp.put(data, temp.get(data)+1);
                    }
                    else { temp.put(data,1);}
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            int maxValue = 0;
            int currentKey = -1;
            for (Map.Entry <Integer, Integer> pair : temp.entrySet()){
                if (pair.getValue()>maxValue){
                    maxValue = pair.getValue();
                    currentKey = pair.getKey();
                }
            }
            resultMap.put(filename, currentKey);
        }
        // implement file reading here - реализуйте чтение из файла тут
    }
}
