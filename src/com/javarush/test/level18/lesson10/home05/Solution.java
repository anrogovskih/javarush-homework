package com.javarush.test.level18.lesson10.home05;

/* Округление чисел
Считать с консоли 2 имени файла
Первый файл содержит вещественные(дробные) числа, разделенные пробелом. Например, 3.1415
Округлить числа до целых и записать через пробел во второй файл
Закрыть потоки. Не использовать try-with-resources
Принцип округления:
3.49 - 3
3.50 - 4
3.51 - 4
-3.49 - -3
-3.50 - -3
-3.51 - -4
*/

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader file1 = new BufferedReader(new FileReader(reader.readLine()));
        FileOutputStream file2 = new FileOutputStream(reader.readLine());
        reader.close();
        ArrayList <Double> array = new ArrayList<>();
        String s = "";
        while (true){
            String temp = file1.readLine();
            if (temp == null) break;
            s = s + temp;
        }
        file1.close();
        String [] sringFile1 = s.split(" ");
        for (int i = 0; i < sringFile1.length; i++)
        {
            array.add(Double.parseDouble(sringFile1[i]));
        }
        ArrayList <Integer> arrayRoundNumbers = new ArrayList<>();
        double temp;
        for (int i = 0; i < array.size(); i++)
        {
            temp = Math.round(array.get(i));
            arrayRoundNumbers.add((int) temp);
        }

        for (int i = 0; i < arrayRoundNumbers.size(); i++)
        {
            file2.write(arrayRoundNumbers.get(i).toString().getBytes());
            file2.write(" ".getBytes());
        }
        file2.close();

    }
}
