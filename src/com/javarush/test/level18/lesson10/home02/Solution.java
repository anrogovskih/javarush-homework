package com.javarush.test.level18.lesson10.home02;

/* Пробелы
В метод main первым параметром приходит имя файла.
Вывести на экран соотношение количества пробелов к количеству всех символов. Например, 10.45
1. Посчитать количество всех символов.
2. Посчитать количество пробелов.
3. Вывести на экран п2/п1*100, округлив до 2 знаков после запятой
4. Закрыть потоки. Не использовать try-with-resources
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.math.RoundingMode;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new FileReader(args [0]));
        String s;
        ArrayList<Character> charList = new ArrayList<>();
        while ((s = br.readLine()) != null){
            char [] temp = s.toCharArray();
            for (int i = 0; i < temp.length; i++)
            {
                charList.add(temp [i]);
            }
        }
        br.close();
        double spaceCount = 0;
        double totalCount = charList.size() +1 ;
        for (int i = 0; i < charList.size(); i++)
        {
            if (charList.get(i) == 32){
                spaceCount++;
            }
        }

        double result =  new BigDecimal(spaceCount/totalCount*100).setScale(2,RoundingMode.HALF_UP).doubleValue();

        System.out.println(result);
    }
}
