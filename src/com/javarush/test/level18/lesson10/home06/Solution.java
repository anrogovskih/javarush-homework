package com.javarush.test.level18.lesson10.home06;

/* Встречаемость символов
Программа запускается с одним параметром - именем файла, который содержит английский текст.
Посчитать частоту встречания каждого символа.
Отсортировать результат по возрастанию кода ASCII (почитать в инете). Пример: ','=44, 's'=115, 't'=116
Вывести на консоль отсортированный результат:
[символ1]  частота1
[символ2]  частота2
Закрыть потоки. Не использовать try-with-resources

Пример вывода:
, 19
- 7
f 361
*/


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        FileInputStream file = new FileInputStream(args[0]);

        Map <Character, Integer> result = new TreeMap<>();
        while (file.available()> 0){
            int data = file.read();
            if (result.containsKey((char) data)){
                result.put((char)data,result.get((char)data)+1);
            }
            else result.put((char)data,1);

        }
        file.close();

        for (Map.Entry <Character, Integer> pair: result.entrySet()){
            System.out.println(pair.getKey() + " " + pair.getValue());
        }
    }
}
