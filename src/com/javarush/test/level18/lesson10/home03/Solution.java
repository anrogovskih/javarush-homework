package com.javarush.test.level18.lesson10.home03;

/* Два в одном
Считать с консоли 3 имени файла
Записать в первый файл содержимого второго файла, а потом дописать в первый файл содержимое третьего файла
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileOutputStream firstFile = new FileOutputStream(reader.readLine());
        FileInputStream secondFile = new FileInputStream(reader.readLine());
        FileInputStream thirdFile = new FileInputStream(reader.readLine());
        reader.close();
        while (secondFile.available()>0){
            int data = secondFile.read();
            firstFile.write(data);
        }
        while (thirdFile.available()>0){
            int data = thirdFile.read();
            firstFile.write(data);
        }
        firstFile.close();
        secondFile.close();
        thirdFile.close();
    }
}
