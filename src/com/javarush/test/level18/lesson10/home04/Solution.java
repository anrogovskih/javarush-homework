package com.javarush.test.level18.lesson10.home04;

/* Объединение файлов
Считать с консоли 2 имени файла
В начало первого файла записать содержимое второго файла так, чтобы получилось объединение файлов
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();
        FileInputStream firstFile = new FileInputStream(file1);
        FileInputStream secondFile = new FileInputStream(file2);
        ArrayList <Byte> firstFileArray = new ArrayList<>();
        ArrayList <Byte> secondFileArray = new ArrayList<>();
        reader.close();
        while (firstFile.available()>0){
            int data = firstFile.read();
            firstFileArray.add((byte) data);
        }
        while (secondFile.available()>0){
            int data = secondFile.read();
            secondFileArray.add((byte) data);
        }
        firstFile.close();
        secondFile.close();
        secondFileArray.addAll(firstFileArray);
        FileOutputStream firstFileOutput = new FileOutputStream(file1);
        for (int i = 0; i < secondFileArray.size(); i++)
        {
            firstFileOutput.write(secondFileArray.get(i));
        }
        firstFileOutput.close();

    }
}
