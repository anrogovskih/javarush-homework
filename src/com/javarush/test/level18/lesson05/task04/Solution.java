package com.javarush.test.level18.lesson05.task04;

/* Реверс файла
Считать с консоли 2 имени файла: файл1, файл2.
Записать в файл2 все байты из файл1, но в обратном порядке
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream firstFile = new FileInputStream(reader.readLine());
        FileOutputStream secondFile = new FileOutputStream(reader.readLine());
        reader.close();
        int[] buffer = new int[firstFile.available()];
        int i = 0;
        while (firstFile.available() >0){
            int data = firstFile.read();
            buffer[i] = data;
            i++;
        }
        for (int j = buffer.length-1; j >-1 ; j--)
        {
            secondFile.write(buffer[j]);
        }
        firstFile.close();
        secondFile.close();
    }
}
