package com.javarush.test.level18.lesson05.task02;

/* Подсчет запятых
С консоли считать имя файла
Посчитать в файле количество символов ',', количество вывести на консоль
Закрыть потоки. Не использовать try-with-resources

Подсказка: нужно сравнивать с ascii-кодом символа ','
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputStream = new FileInputStream(reader.readLine());
        reader.close();
        ArrayList <Byte> bytes = new ArrayList<>();
        while (inputStream.available() > 0) {
            //читаем весь файл одним куском
            int count = inputStream.read();
            bytes.add((byte) count);
        }
        inputStream.close();
        int count = 0;
        for (Byte aByte : bytes)
        {
            if (aByte == 44){
                count++;
            }
        }
        System.out.println(count);
    }
}
