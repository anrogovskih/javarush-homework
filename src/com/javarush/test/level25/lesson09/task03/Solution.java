package com.javarush.test.level25.lesson09.task03;


import java.util.ArrayList;

/* Живем своим умом
В классе Solution реализуйте интерфейс UncaughtExceptionHandler, который должен:
1. прервать нить, которая бросила исключение.
2. вывести в консоль стек исключений начиная с самого вложенного.
Пример исключения: new Exception("ABC", new RuntimeException("DEF", new IllegalAccessException("GHI")))
Пример вывода:
java.lang.IllegalAccessException: GHI
java.lang.RuntimeException: DEF
java.lang.Exception: ABC
*/
public class Solution implements Thread.UncaughtExceptionHandler {
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        t.interrupt();
        Throwable result = e.getCause();
        ArrayList<Throwable> list = new ArrayList<>();
        list.add(e);
        while (result != null){
            list.add(result);
            result = cause(result);
        }
        for (int i = list.size()-1; i > -1; i--)
        {
            System.out.println(list.get(i));
        }
    }
    public Throwable cause(Throwable e){
        return e.getCause();
    }
    public static void main(String[] args){
        Throwable e =  new Exception("ABC", new RuntimeException("DEF", new IllegalAccessException("GHI")));
        new Solution().uncaughtException(Thread.currentThread(), e);
    }
}
