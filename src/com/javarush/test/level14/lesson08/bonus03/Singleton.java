package com.javarush.test.level14.lesson08.bonus03;

/**
 * Created by Анатолий on 14.07.2016.
 */
public class Singleton
{
    private static Singleton singleton;
    private Singleton()
    {
    }

    public static Singleton getInstance(){
        if (singleton == null){
            singleton = new Singleton();
        }
        return singleton;
    }
}
