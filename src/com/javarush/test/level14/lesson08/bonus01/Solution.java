package com.javarush.test.level14.lesson08.bonus01;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* Нашествие эксепшенов
Заполни массив exceptions 10 различными эксепшенами.
Первое исключение уже реализовано в методе initExceptions.
*/

public class Solution
{
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args)
    {
        initExceptions();

        for (Exception exception : exceptions)
        {
            System.out.println(exception);
        }
    }

    private static void initExceptions()
    {   //it's first exception
        try
        {
            float i = 1 / 0;

        }
        catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            float i = 1 / 0;

        }
        catch (RuntimeException e)
        {
            exceptions.add(e);
        }
        try
        {
            float i = 1 / 0;

        }
        catch (ArithmeticException e)
        {
            exceptions.add(e);
        }
        try
        {
            BufferedReader fileReader = new BufferedReader(new FileReader("123"));

        }
        catch (FileNotFoundException e)
        {
            exceptions.add(e);
        }
        try
        {
            BufferedReader fileReader = new BufferedReader(new FileReader("123"));
            String content = fileReader.readLine();

        }
        catch (IOException e)
        {
            exceptions.add(e);
        }
        try
        {

            String content = null;
            content = content.substring(1);

        }
        catch (NullPointerException e)
        {
            exceptions.add(e);
        }
        try
        {
            String [] strings = new String[2];
            strings[0] = "1";
            strings[1] = "2";
            strings[3] = "3";
        }
        catch (IndexOutOfBoundsException e)
        {
            exceptions.add(e);
        }
        try
        {

            String content = "Z";
            int test = Integer.parseInt(content);

        }
        catch (NumberFormatException e)
        {
            exceptions.add(e);
        }
        try
        {

            String content = "Z";
            int test = Integer.parseInt(content);

        }
        catch (IllegalArgumentException e)
        {
            exceptions.add(e);
        }
        try
        {
            throw new CloneNotSupportedException();

        }
        catch (CloneNotSupportedException e)
        {
            exceptions.add(e);
        }


        //Add your code here

    }
}
