package com.javarush.test.level23.lesson13.big01;

public class SnakeSection
{
    @Override
    public int hashCode()
    {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == this){
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()){
            return false;
        }
        SnakeSection snakeSection = (SnakeSection) obj;
        if (x != snakeSection.x) return false;
        return y == snakeSection.y;
    }

    private int x;
    private int y;


    public SnakeSection(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }
}
